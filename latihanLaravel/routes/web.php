<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use PhpParser\Node\Expr\Cast;
use PhpParser\Node\Expr\FuncCall;
use App\Http\Controllers\CastController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index']);
Route::get('/form', [AuthController::class, 'regist']);
Route::post('/welcome', [AuthController::class, 'rspnRegist']);
Route::get('/table', function(){
    return view('table');
});
Route::get('/data-table', function(){
    return view('data-table');
});

// CRUD CAST
// Create
// Route mengarah ke halaman tambah cast
Route::get('/cast/create',[CastController::class, 'create']);
// Route untuk menyimpan inputan masuk ke dalam DB & validasi
Route::post('/cast', [CastController::class, 'store']);

// Read
// Menampilkan semua data di DB di browser
Route::get('/cast', [CastController::class, 'index']);
// Detail berdasarkan ID
Route::get('/cast/{id}', [CastController::class, 'show']);

// Update Data
// Mengarah ke halaman update data berdasarkan ID
Route::get('/cast/{id}/edit', [CastController::class,'edit']);
// update data ke DB berdasarkan ID
Route::put('/cast/{id}', [CastController::class, 'update']);

// Delete Data
// Delete data berdasarkan ID
Route::delete('cast/{id}',[CastController::class, 'destroy']);

?>