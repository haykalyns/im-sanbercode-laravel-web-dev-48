@extends('layout.master')
@section('pageName')
    Detail Cast
@endsection
@section('content')

<h1 class="text-primary">{{$castData->nama}}</h1>
<p>Umur dari {{$castData->nama}} adalah {{$castData->umur}}</p>
<h2>{{$castData->nama}} Biography</h2>
<p>{{$castData->bio}}</p>
@endsection