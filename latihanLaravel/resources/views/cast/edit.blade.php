@extends('layout.master')
@section('pageName')
    Edit Cast
@endsection
@section('content')
<form method="POST" action="/cast/{{$castData->id}}">
    @csrf
    @method('put')
    <div class="form-group">
      <label>Nama</label>
      <input type="text" name="nama" value="{{$castData->nama}}" class="form-control @error('nama') is-invalid @enderror" placeholder="Masukkan Nama">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label >Umur</label>
      <input type="number" name="umur" value="{{$castData->umur}}" class="form-control @error('umur') is-invalid @enderror" placeholder="Umur">
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label >Bio</label>
        <textarea name="bio" id="" cols="30" rows="10" class="form-control @error('bio') is-invalid @enderror">{{$castData->bio}}</textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection