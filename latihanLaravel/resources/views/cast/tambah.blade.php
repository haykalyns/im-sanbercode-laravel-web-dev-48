@extends('layout.master')
@section('pageName')
    Tambah Cast
@endsection
@section('content')
<form method="POST" action="/cast">
    @csrf
    <div class="form-group">
      <label>Nama</label>
      <input type="text" name="nama" class="form-control @error('nama') is-invalid @enderror" placeholder="Masukkan Nama">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label >Umur</label>
      <input type="number" name="umur" class="form-control @error('umur') is-invalid @enderror" placeholder="Umur">
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label >Bio</label>
        <textarea name="bio" id="" cols="30" rows="10" class="form-control @error('bio') is-invalid @enderror"></textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection