@extends('layout.master')
@section('pageName')
    Data Cast
@endsection
@section('content')
<a href="/cast/create" class="btn btn-success btn-sm my-2">Tambah Cast</a>
<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key => $item)
        <tr>
            <th scope="row">{{$key+1}}</th>
            <td>{{$item->nama}}</td>
            <td>
                <form action="/cast/{{$item->id}}" method="post">
                    @csrf
                    @method('delete')
                    <a href="/cast/{{$item->id}}" class="btn btn-secondary btn-sm">Detail</a>
                    <a href="/cast/{{$item->id}}/edit" class="btn btn-primary btn-sm">Edit</a>
                    <input type='submit' class="btn btn-danger btn-sm" value="delete">
                </form>
            </td>
          </tr>
        @empty
            <td>Tidak Ada Cast</td>
        @endforelse
    </tbody>
  </table>

@endsection