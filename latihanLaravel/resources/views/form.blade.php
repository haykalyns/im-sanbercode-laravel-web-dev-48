@extends('layout.master')
@section('pageName')
Pendaftaran Akun
@endsection

@section('content')
<html>
    <body>
        <h1>Buat Account Baru!</h1>
        <h3>Sign Up Form</h3>
        <form action="/welcome" method="post"> 
            @csrf
            <label>First name:</label><br><br>
            <input type="text" name="first_name"><br><br>

            <label>Last name:</label><br><br>
            <input type="text" name="last_name"><br><br>

            <label>Gender:</label><br><br>
            <input type="radio">Male<br>
            <input type="radio">Female<br>
            <input type="radio">Other<br><br>
            
            <label>Nationality:</label><br><br>
            <select name="nationality">
                <option value="Indonesian">Indonesian</option>
                <option value="Spanish">Spanish</option>
                <option value="Swedish">Swedish</option>
            </select>
            <br><br>

            <label>Language Spoken:</label><br><br>
            <input type="checkbox">Bahasa Indonesia<br>
            <input type="checkbox">English<br>
            <input type="checkbox">Other<br><br>

            <label>Bio:</label><br><br>
            <textarea name="message" rows="15" cols="30"></textarea><br><br>
            <button type="submit">Sign Up</button>

        </form>
    </body>
</html>
@endsection