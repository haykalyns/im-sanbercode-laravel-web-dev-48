<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function regist()
    {
        return view('form');
    }

    public function rspnRegist(Request $request)
    {
        $namaDepan = $request->input('first_name');
        $namaBelakang = $request->input('last_name');

        return view('welcome', ['namaDepan'=>$namaDepan, 'namaBelakang' => $namaBelakang]);
    }
}
