<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function create()
    {
        return view('cast.tambah');
    }

    public function store(Request $request)
    {
        // dd($request->all());
        // Validasi
        $request->validate([
            'nama' => 'required|max:45',
            'umur' => 'required',
            'bio' => 'required',
        ]);
        // Insert Data ke DB
        DB::table('cast')->insert([
            'nama' => $request->input('nama'),
            'umur' => $request->input('umur'),
            'bio' => $request->input('bio')
        ]);

        // mengarahkan ke halaman /cast
        return redirect('/cast');
    }

    public function index()
    {
        $cast = DB::table('cast')->get();
        return view('cast.index', ['cast' => $cast]);
    }

    public function show($id)
    {
        $castData = DB::table('cast')->find($id);

        return view('cast.detail', ['castData' => $castData]);
    }
    
    public function edit($id)
    {
        $castData = DB::table('cast')->find($id);

        return view('cast.edit', ['castData' => $castData]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required|max:45',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        // Update Data
        DB::table('cast')
              ->where('id', $id)
              ->update(
                [
                    'nama' => $request->input('nama'),
                    'umur' => $request->input('umur'),
                    'bio' => $request->input('bio')
                ]
            );
            // redirect alamat
            return redirect('/cast');
    }

    public function destroy($id)
    {
        DB::table('cast')->where('id', '=', $id)->delete();

        return redirect('/cast');
    }
}
